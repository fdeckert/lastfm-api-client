use std::collections::HashMap;

use crate::constants;
use crate::types;
use crate::AuthClient;

#[derive(Debug)]
pub struct BlankClient {
    api_key: String,
    shared_secret: String,
}

impl BlankClient {
    pub fn new(api_key: String, shared_secret: String) -> Self {
        Self {
            api_key,
            shared_secret,
        }
    }

    pub async fn get_user_auth_token(&self) -> Result<String, String> {
        let parameters = vec![
            ("api_key", String::from(&self.api_key)),
            ("format", "json".to_owned()),
            ("method", "auth.getToken".to_owned()),
        ];

        let mut response = reqwest::Client::new()
            .get(constants::API_URL)
            .query(&parameters)
            .send()
            .await
            .unwrap()
            .json::<HashMap<String, String>>()
            .await
            .unwrap();

        match response.remove("token") {
            Some(token) => Ok(token),
            None => Err("eh".to_owned()),
        }
    }

    pub async fn to_auth(&self, token: &str) -> Result<AuthClient, types::Error> {
        let mut parameters: Vec<(String, String)> = vec![
            ("api_key".to_string(), String::from(&self.api_key)),
            ("method".to_string(), "auth.getSession".to_string()),
            ("token".to_string(), token.to_string()),
        ];

        Self::generate_sig(&mut parameters, &self.shared_secret);

        parameters.push(("format".to_string(), "json".to_string()));

        let response_str: String = reqwest::Client::new()
            .get(constants::API_URL)
            .query(&parameters)
            .send()
            .await
            .unwrap()
            .text()
            .await
            .unwrap();

        match serde_json::from_str::<types::AuthResponseResult>(&response_str).unwrap() {
            types::AuthResponseResult::Ok(result) => Ok(AuthClient::new(
                String::from(&self.api_key),
                result.session.key,
                result.session.name,
            )),
            types::AuthResponseResult::Err(e) => Err(e),
        }
    }

    pub fn get_api_key(&self) -> &str {
        &self.api_key
    }

    fn generate_sig(params: &mut Vec<(String, String)>, secret: &str) {
        params.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());

        println!("Generating signature for params :\n{:?}", &params);

        let mut concat = String::new();

        for key_value in params.iter() {
            concat.push_str(&key_value.0);
            concat.push_str(&key_value.1);
        }
        concat.push_str(secret);

        println!("Concatenated version :\n{}", concat);

        let sig = format!("{:x}", md5::compute(concat));
        println!("Generated signature :\n{}", sig);
        params.push(("api_sig".to_string(), sig));
    }
}
