use crate::constants;
use crate::types;

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthClient {
    api_key: String,
    session_key: String,
    username: String,
}

impl AuthClient {
    pub fn new(api_key: String, session_key: String, username: String) -> Self {
        AuthClient {
            api_key,
            session_key,
            username,
        }
    }

    pub fn get_username(&self) -> &str {
        &self.username
    }

    pub async fn get_user_info(&self) -> Result<types::User, String> {
        let params = vec![
            ("api_key", String::from(&self.api_key)),
            ("user", String::from(&self.username)),
            ("method", "user.getInfo".to_owned()),
            ("format", "json".to_owned()),
        ];
        let response = reqwest::Client::new()
            .get(constants::API_URL)
            .query(&params)
            .send()
            .await
            .unwrap()
            .text()
            .await
            .unwrap();

        println!("{}", response);

        Ok(serde_json::from_str::<types::UserInfo>(&response).unwrap().user)
    }

    pub async fn get_time_intervals(&self) -> Result<Vec<types::TimePeriod>, String> {
        let params = vec![
            ("user", String::from(&self.username)),
            ("api_key", String::from(&self.api_key)),
            ("format", "json".to_owned()),
            ("method", "user.getWeeklyChartList".to_owned()),
        ];

        let response = reqwest::Client::new()
            .get(constants::API_URL)
            .query(&params)
            .send()
            .await
            .unwrap()
            .text()
            .await
            .unwrap();

        let v: serde_json::Value = serde_json::from_str(&response).unwrap();
        let obj = v.as_object().unwrap();
        let list = obj.get("weeklychartlist").unwrap().as_object().unwrap();
        let charts = list.get("chart").unwrap().as_array().unwrap();

        let mut periods: Vec<types::TimePeriod> = Vec::new();

        for chart in charts {
            let chart = chart.as_object().unwrap();
            let from: u32 = chart
                .get("from")
                .unwrap()
                .as_str()
                .unwrap()
                .parse()
                .unwrap();
            let to: u32 = chart.get("to").unwrap().as_str().unwrap().parse().unwrap();

            periods.push(types::TimePeriod { from, to });
        }

        Ok(periods)
    }

    pub async fn get_top_tracks_period(
        &self,
        from: u32,
        to: u32,
        count: usize,
    ) -> Result<Vec<types::Track>, String> {
        let from_str = from.to_string();
        let to_str = to.to_string();

        let params = vec![
            ("user", String::from(&self.username)),
            ("api_key", String::from(&self.api_key)),
            ("format", "json".to_owned()),
            ("method", "user.getWeeklyTrackChart".to_owned()),
            ("from", from_str),
            ("to", to_str),
        ];

        let response = reqwest::Client::new()
            .get(constants::API_URL)
            .query(&params)
            .send()
            .await
            .unwrap()
            .text()
            .await
            .unwrap();

        let root: serde_json::Value = serde_json::from_str(&response).unwrap();
        let weeklytrack = root
            .as_object()
            .unwrap()
            .get("weeklytrackchart")
            .unwrap()
            .as_object()
            .unwrap();
        let tracks = weeklytrack.get("track").unwrap().as_array().unwrap();

        let mut top_tracks: Vec<types::Track> = Vec::new();

        for (i, track) in tracks.iter().enumerate() {
            if i >= count {
                break;
            }

            let track = track.as_object().unwrap();
            let name = track.get("name").unwrap().as_str().unwrap();
            let artist = track
                .get("artist")
                .unwrap()
                .as_object()
                .unwrap()
                .get("#text")
                .unwrap()
                .as_str()
                .unwrap();
            let position: u32 = track
                .get("@attr")
                .unwrap()
                .as_object()
                .unwrap()
                .get("rank")
                .unwrap()
                .as_str()
                .unwrap()
                .parse()
                .unwrap();
            let listen_count: u32 = track
                .get("playcount")
                .unwrap()
                .as_str()
                .unwrap()
                .parse()
                .unwrap();
            top_tracks.push(types::Track {
                name: String::from(name),
                artist: String::from(artist),
                position,
                listen_count,
            });
        }
        Ok(top_tracks)
    }

}

impl Clone for AuthClient {
    fn clone(&self) -> Self {
        Self{
            api_key: String::from(&self.api_key),
            session_key: String::from(&self.session_key),
            username: String::from(&self.username),
        }
    }
}
