use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct TimePeriod {
    pub from: u32,
    pub to: u32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Track {
    pub name: String,
    pub artist: String,
    pub position: u32,
    pub listen_count: u32,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum AuthResponseResult {
    Ok(AuthResponse),
    Err(Error),
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Error {
    error: u32,
    message: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct AuthResponse {
    pub session: Session,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Session {
    pub key: String,
    pub name: String,
    pub subscriber: u32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserInfo {
    pub user: User,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    pub name: String,
    pub realname: String,
    pub url: String,
    pub image: Vec<Image>,
    pub country: String,
    pub age: String,
    pub gender: String,
    pub subscriber: String,
    pub playcount: String,
    pub playlists: String,
    pub bootstrap: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Image {
    pub size: String,
    #[serde(rename(deserialize = "#text"))]
    pub text: String,
}
